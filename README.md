# phenora

An R package to analyze 'phenocam' images for greenness and other metrics.

To install, use the `devtools` package:

```
install.packages("devtools")  # perhaps not needed
devtools::install_bitbucket("remkoduursma/phenora")
```

*This package was developed by the ACEAS working group on the Australian Phenology Network*

Developer and contact : Remko Duursma <remkoduursma@gmail.com>


## Example

```
# Set image stack to a folder that contains your pheno images
myimgs <- setImages("path/to/images")

# Select a region of interest, by clicking on an image (works on Windows only, for now)
# Choose the ROI on the first image in the stack
myroi <- selectROI(myimgs,1)

# Process for greenness
processImages(myimgs, ROI=myroi)
```


